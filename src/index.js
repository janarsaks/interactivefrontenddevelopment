import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import createRootReducer from './reducers';
import App from './containers/App';
import gameLobbyMiddleware from './middlewares/GameLobbyMiddleware';
import {createBrowserHistory} from 'history';
import {routerMiddleware} from 'connected-react-router';

const composeStoreEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const history = createBrowserHistory();

const store = createStore(
  createRootReducer(history),
  composeStoreEnhancers(
    applyMiddleware(
      routerMiddleware(history),
      thunk,
      gameLobbyMiddleware
    )
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App history={history}/>
  </Provider>,
  document.getElementById('root')
);
