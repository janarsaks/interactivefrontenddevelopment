import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {isInFlight} from '../reducers/GamesReducer';

export const GameInFlight = () => <div>Game is being created...</div>;

const Games = (props) => {
  const games = props.games.map((game, index) => {
    if (isInFlight(game)) {
      return <GameInFlight key={index} />;
    } else {
      return (
        <li key={game.id}>
          <Link to={`/games/${game.id}`}>Game with frequency: {game.frequency}ms</Link>
        </li>
      );
    }
  });

  return (
    <div className="games">
      <ul className="games-list">{games}</ul>
    </div>
  );
};

Games.propTypes = {
  games: PropTypes.array.isRequired
};

export default Games;
