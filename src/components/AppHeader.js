import React from 'react';
import {Link} from 'react-router-dom';

const Header = () => {
  return (
    <div className='app-header'>
      <ul>
        <li><Link to="/createGame">Create game</Link></li>
        <li><Link to="/players">Online players</Link></li>
        <li><Link to="/ongoingGames">Ongoing games</Link></li>
        <li><Link to="/closedGames">Closed games</Link></li>
      </ul>
    </div>
  );
};

export default Header;
