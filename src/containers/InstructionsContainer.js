import React from 'react';
import Instructions from '../components/Instructions';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {totalMiss, averageMissPercentage} from '../reducers/GamesReducer';

const InstructionsContainer = (props) => {
  return (
    <Instructions
      name={props.playerName}
      totalMiss={props.totalMiss}
      averageMissPercentage={props.averageMissPercentage}
    />
  );
};

InstructionsContainer.propTypes = {
  totalMiss: PropTypes.number.isRequired,
  averageMissPercentage: PropTypes.number.isRequired,
  playerName: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({
  playerName: state.connection.playerName,
  totalMiss: totalMiss(state.games),
  averageMissPercentage: averageMissPercentage(state.games)
});

export default connect(mapStateToProps)(InstructionsContainer);
