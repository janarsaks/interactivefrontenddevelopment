import React, {Fragment} from 'react';
import NewGameForm from '../components/NewGameForm';
import Disconnect from '../components/Disconnect';
import {connect} from 'react-redux';
import {newGame, DISCONNECT_REQUESTED} from '../actions/Actions';
import PropTypes from 'prop-types';

const Controls = (props) => {
  return (
    <Fragment>
      <Disconnect disconnect={props.disconnect} />
      <NewGameForm onStart={props.newGame} />
    </Fragment>
  );
};

Controls.propTypes = {
  newGame: PropTypes.func.isRequired,
  disconnect: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  games: state.games
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  newGame: ({frequency}) => {
    dispatch(newGame(frequency));
    ownProps.history.push('/ongoingGames');
  },
  disconnect: () => dispatch({type: DISCONNECT_REQUESTED})
});

export default connect(mapStateToProps, mapDispatchToProps)(Controls);
