import {connect} from 'react-redux';
import Games from '../components/Games';
import {openGames} from '../reducers/GamesReducer';

const mapStateToProps = (state, props) => {
  if (props.status === 'open') {
    return {games: openGames(state.games)};
  }
  return {games: state.games.filter((game) => game.status === props.status)};
};

export default connect(mapStateToProps)(Games);
