import React from 'react';
import {CLOSE_GAME, recordHit} from '../actions/Actions';
import MetronomeGame from '../components/MetronomeGame';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const GameOrNotFound = ({game, gameId, recordHit, closeGame}) => {
  if (game) {
    return <MetronomeGame
      key={game.id}
      frequencyMs={game.frequency}
      hits={game.hits}
      onHit={() => recordHit({gameId})}
      onClose={() => closeGame({gameId})}
      status={game.status}
    />;
  } else {
    return <p>Game {gameId} not found</p>;
  }
};
GameOrNotFound.propTypes = {
  game: PropTypes.object,
  gameId: PropTypes.string.isRequired,
  closeGame: PropTypes.func.isRequired,
  recordHit: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const gameId = ownProps.match.params.gameId;
  const game = state.games.find((game) => game.id === gameId);
  return {game: game, gameId: gameId};
};

const mapDispatchToProps = (dispatch) => ({
  closeGame: ({gameId}) => dispatch({type: CLOSE_GAME, payload: gameId}),
  recordHit: ({gameId}) => dispatch(recordHit(gameId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GameOrNotFound);
