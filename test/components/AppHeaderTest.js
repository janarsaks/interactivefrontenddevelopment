import React from 'react';
import {shallow} from 'enzyme';

import AppHeader from '../../src/components/AppHeader';

describe('AppHeader', () => {
  it('renders successfully', () => {
    expect(shallow(<AppHeader />))
      .to.exist;
  });
});
