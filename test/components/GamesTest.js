import React from 'react';
import {shallow} from 'enzyme';

import Games, {GameInFlight} from '../../src/components/Games';
import {Link} from 'react-router-dom';

describe('Games', () => {
  it('renders successfully', () => {
    expect(shallow(
      <Games
        games={[]}
      />
    )).to.exist;
  });

  it('renders in flight games', () => {
    const games = shallow(
      <Games
        games={[{status: 'inFlight'}]}
      />
    );
    expect(games).to.have.exactly(1).descendants(GameInFlight);
  });

  it('renders games as links', () => {
    const games = shallow(
      <Games
        games={[{id: 1, status: 'open', hits: [], frequency: 5}]}
      />
    );
    expect(games).to.have.exactly(1).descendants(Link);
  });
});
